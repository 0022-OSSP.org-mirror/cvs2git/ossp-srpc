#ifndef __LIBAMP_H__
#define __LIBAMP_H__

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>

typedef struct
    {
    int (*listen)(int fd, int backlog);
    int (*accept)(int fd, struct sockaddr* addr, socklen_t* addrlen);
    int (*bind)(int fd, const struct sockaddr* addr, socklen_t addrlen);
    int (*connect)(int fd, const struct sockaddr* name, socklen_t namelen);
    ssize_t (*read)(int fd, void* buf, size_t nbytes);
    ssize_t (*readv)(int fd, const struct iovec* iov, int iovcnt);
    ssize_t (*write)(int fd, const void* buf, size_t nbytes);
    ssize_t (*writev)(int fd, const struct iovec* iov, int iovcnt);
    ssize_t (*send)(int s, const void* msg, size_t len, int flags);
    ssize_t (*sendto)(int s, const void* msg, size_t len, int flags,
		      const struct sockaddr* to, socklen_t tolen);
    ssize_t (*recv)(int s, void* buf, size_t len, int flags);
    ssize_t (*recvfrom)(int s, void* buf, size_t len, int flags, struct sockaddr* from,
			socklen_t* fromlen);
    }
amp_engine_t;

typedef struct
    {
    int fd;
    amp_engine_t engine;
    }
amp_peer_t;

amp_peer_t* amp_create_message_port(const char* peer, amp_engine_t* engine);
int amp_destroy_message_port(amp_peer_t* peer);

amp_peer_t* amp_create_peer(const char* peer, const char* engine);
int amp_destroy_peer(amp_peer_t* peer);

int amp_peer2fd(amp_peer_t* peer);
amp_peer_t* amp_fd2peer(int fd);

int amp_send(amp_peer_t* peer, const void* msg, size_t msg_len);
int amp_receive(amp_peer_t* peer, const void** msg, size_t* msg_len);

#endif /* !defined(__LIBAMP_H__) */
